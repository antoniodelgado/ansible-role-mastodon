## ansible-role-mastodon

# Credits

Thanks for this post https://sleeplessbeastie.eu/2022/05/02/how-to-take-advantage-of-docker-to-install-mastodon/ that helped a lot

# Description

This Ansible role would install a Mastodon instance either directly in the computer or a set of docker containers using docker-compose. You will need to have a reverse proxy pointing then to ports 3000 and 4000.

# Configuration

Check the default/main.yml for used variables and customize. If you don't have secrets already generated, leave them empty and the role will create them and you can pick them up from the application.env.production file created in the Mastodon folder.

# To-Do

- Is federation working?
- Test bare metal installation (so far only tested with docker)
- There are 500 errors on my instance when accessing "Administration"
- Include reverse proxy web server (nginx and apache)
